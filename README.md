# README

To get this up and running:

* run 'bundle install'
* run 'rails db:migrate'
* run 'rails db:seed'
* run 'rails s', go to localhost:3000
