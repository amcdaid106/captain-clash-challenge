class Player < ApplicationRecord
  mount_uploader :avatar, AvatarUploader

  validates_presence_of :name, :avatar, :life_points, :attack_points
  validates :life_points, numericality: {
                                          less_than_or_equal_to: 20,
                                          greater_than_or_equal_to: 5,
                                          only_integer: true
                                        }
  validates :attack_points, numericality: {
                                          less_than_or_equal_to: 5,
                                          greater_than_or_equal_to: 1,
                                          only_integer: true
                                        }
end

