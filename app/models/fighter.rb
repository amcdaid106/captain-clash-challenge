class Fighter < ApplicationRecord
  belongs_to :battle
  belongs_to :player
  belongs_to :weapon
  belongs_to :shield
end
