class BattlesController < ApplicationController
  def index
    @battles = Battle.all
  end

  def new
    @players = Player.all
    @weapons = Weapon.all
    @shields = Shield.all
  end

  def create
    battle = Battle.create
    fighters = params[:fighters]
    add_fighter(battle, fighters["0"])
    add_fighter(battle, fighters["1"])
    respond_to do |format|
      format.html { redirect_to battle_path(battle) }
    end
  end

  def add_fighter(battle, fighter_params)
    fighter = Fighter.new
    fighter.battle = battle
    fighter.player = Player.find(fighter_params[:player_id])
    fighter.weapon = Weapon.find(fighter_params[:weapon_id])
    fighter.shield = Shield.find(fighter_params[:shield_id])
    fighter.save
  end

  def show
    @battle = Battle.find(params[:id])
    @fighters = construct_battle_fighters(@battle)
  end

  def set_result
    @battle = Battle.find(params[:battle_id])
    f1 = @battle.fighters[0]
    f1.update(
      remaining_life_points: params[:fighter_one_points],
      winner: (params[:fighter_one_points] > params[:fighter_two_points])
    )
    f2 = @battle.fighters[1]
    f2.update(
      remaining_life_points: params[:fighter_two_points],
      winner: (params[:fighter_two_points] > params[:fighter_one_points])
    )
  end

  def construct_battle_fighters(battle)
    battle.fighters.map do |fighter|
      {
        id: fighter.id,
        name: fighter.player.name,
        life_points: fighter.player.life_points,
        image_url: fighter.player.avatar.url,
        attack_points: fighter.player.attack_points + fighter.weapon.hit_points,
        defense_points: fighter.shield.defense_points
      }
    end
  end
end


