class PlayersController < ApplicationController
  def new
    @player = Player.new
  end

  def create
    @player = Player.new(player_params)
    respond_to do |format|
      if @player.save
        format.html { redirect_to new_battle_path }
      else
        format.html { render '/players/new' }
      end
    end
  end

  private

    def player_params
      params.require(:player).permit(
        :name,
        :life_points,
        :attack_points,
        :avatar,
        :avatar_image_cache
      )
    end

end
