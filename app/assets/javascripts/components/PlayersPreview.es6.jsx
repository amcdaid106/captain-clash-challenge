class PlayersPreview extends React.Component {
  getPreviousPlayer(currentPlayerIndex, players) {
    if (currentPlayerIndex > 0) {
      return players[currentPlayerIndex - 1]
    } else {
      return players[players.length - 1]
    }
  }

  getNextPlayer(currentPlayerIndex, players) {
    if (currentPlayerIndex === players.length - 1) {
      return players[0]
    } else {
      return players[currentPlayerIndex + 1]
    }
  }

  visiblePlayers() {
    ({players, currentIndex} = this.props);
    const previousPlayer = this.getPreviousPlayer(currentIndex, players);
    const nextPlayer = this.getNextPlayer(currentIndex, players);
    return [previousPlayer, players[currentIndex], nextPlayer]
  }

  render() {
    const stage = this.props.stage;
    const players = this.visiblePlayers();
    return (
      <div>
        <div
          className={classNames(
            "players-preview",
            {"hide": (stage === "fighters_selected")}
          )}
        >
          <div
            className="player player-left"
            onClick={(e) => this.props.onSelect('left')}
          >
            <i className="fa fa-chevron-left"></i>
            <img src={players[0].avatar.url} alt=""/>
          </div>
          <div
            className="player player-middle"
          ><img src={players[1].avatar.url} alt=""/></div>
          <div
            className="player player-right"
            onClick={(e) => this.props.onSelect('right')}
          >
            <img src={players[2].avatar.url} alt=""/>
            <i className="fa fa-chevron-right"></i>
          </div>
        </div>
      </div>
    )
  }
}
