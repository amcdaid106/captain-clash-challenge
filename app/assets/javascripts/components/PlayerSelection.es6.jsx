class PlayerSelection extends React.Component {
  render() {
    const stage = this.props.stage;
    ({selectedPlayer, playerOne, playerTwo, selectedWeapon, selectedShield} = this.props);
    return (
      <div className="player-select__container">
        <div className="player-selected__wrapper one">
          <h5 className="default-text-shadow">Personnage 1</h5>
          <div className="player-one__selected">
            { playerOne ?
              <img src={playerOne.avatar.url} alt=""/>
            : '?'
            }
          </div>
        </div>
        <div className="player-and-items-select">
          <div
            className={classNames(
              'player-select__inner-container',
              {'hide': (stage === 'fighters_selected')}
            )}
          >
            <div className="inner-container__content">
              <img className="player-select__image" src={selectedPlayer.avatar.url} alt=""/>
              <div className="player-select__details">
                <h2 className="player-select__name">{selectedPlayer.name}</h2>
                <p>Points de vie : {selectedPlayer.life_points}</p>
                <p>Points d'attaque (de base + d'arme): {selectedPlayer.attack_points + selectedWeapon.hit_points}</p>
                <p>Points de défense : {selectedShield.defense_points}</p>
              </div>
              <ItemChooser
                title="Armes"
                items={this.props.weapons}
                selectedItem={selectedWeapon}
                onSelected={this.props.onWeaponSelected}
                getTooltip={(weapon) => `Points d'attaque: ${weapon.hit_points}`}
              />
              <ItemChooser
                title="Boucliers"
                items={this.props.shields}
                selectedItem={selectedShield}
                onSelected={this.props.onShieldSelected}
                getTooltip={(shield) => `Points de defense: ${shield.defense_points}`}
              />
            </div>
          </div>
          <div>
            <button
              onClick={this.props.setFighter}
              className={classNames({'hide': (stage === 'fighters_selected')}
            )}>Affecter personnage</button>
          </div>
        </div>
        <div className="player-selected__wrapper two">
          <h5 className="default-text-shadow">Personnage 2</h5>
          <div className="player-two__selected">
            { playerTwo ?
              <img src={playerTwo.avatar.url} alt=""/>
            : '?'
            }
          </div>
        </div>
      </div>
    )
  }
}
