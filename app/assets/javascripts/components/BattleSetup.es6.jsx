class BattleSetup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      playerOne: null,
      playerTwo: null,
      selectedPlayerIndex: 0,
      stage: 'select_fighter_one',
      selectedWeapon: this.props.weapons[0],
      selectedShield: this.props.shields[0],
      fighters: []
    }
  }

  changeSelectedPlayer(direction) {
    const currentIndex = this.state.selectedPlayerIndex;
    const lastIndex = (this.props.players.length - 1);
    const newIndex = (direction === 'left') ?
      this.getPreviousIndex(currentIndex, lastIndex) :
      this.getNextIndex(currentIndex, lastIndex)
    this.setState({selectedPlayerIndex: newIndex});
  }

  getPreviousIndex(currentIndex, lastIndex) {
    return (currentIndex === 0 ? lastIndex : currentIndex - 1);
  }

  getNextIndex(currentIndex, lastIndex) {
    return (currentIndex === lastIndex ? 0 : currentIndex + 1);
  }

  setHeaderText(stage) {
    switch (stage) {
      case 'select_fighter_one':
        return "Choix du personnage 1";
      case 'select_fighter_two':
        return "Choix du personnage 2";
      case 'fighters_selected':
        return "Personnages choisis";
    }
  }

  setFighter() {
    ({stage} = this.state);
    let fighter = null;
    const visiblePlayer = this.props.players[this.state.selectedPlayerIndex];
    if (stage === 'select_fighter_one') {
      this.setState({
        stage: 'select_fighter_two',
        playerOne: visiblePlayer,
        selectedWeapon: this.props.weapons[0],
        selectedShield: this.props.shields[0]
      });
      fighter = this.props.fighterOne;
    } else if (stage === 'select_fighter_two') {
      this.setState({
        stage: 'fighters_selected',
        playerTwo: visiblePlayer
      });
      fighter = this.props.fighterTwo;
    }
    this.addFighter(
      fighter,
      visiblePlayer,
      this.state.selectedWeapon,
      this.state.selectedShield
    );
  }

  handleWeaponSelection(weapon) {
    this.setState({selectedWeapon: weapon});
  }

  handleShieldSelection(shield) {
    this.setState({selectedShield: shield});
  }

  addFighter(fighter, player, weapon, shield) {
    this.state.fighters.push({
      player_id: player.id,
      weapon_id: weapon.id,
      shield_id: shield.id
    });
  }

  createBattle() {
    $.post('/battles', {
      fighters: this.state.fighters
    });
  }

  render() {
    ({stage} = this.state);
    const headerText = this.setHeaderText(stage);
    const visiblePlayer = this.props.players[this.state.selectedPlayerIndex];
    return (
      <div className="battle-setup__container">
        <a
          href={this.props.backLink}
          className="go-back-link navigation-link default-text-shadow"
        >
          <i className="fa fa-arrow-left" aria-hidden="true"></i>
          Retourner à l'arène
        </a>
        <h1 className="battle-setup__header default-text-shadow">{headerText}</h1>
        <a
          href={this.props.newPlayerLink}
          className={classNames(
            "new-player-link",
            "navigation-link",
            "default-text-shadow",
            {"hide": (stage === "fighters_selected")}
          )}
        >
          Créer un nouveau personnage
          <i className="fa fa-plus" aria-hidden="true"></i>
        </a>
        <div>
          <PlayersPreview
            players={this.props.players}
            currentIndex={this.state.selectedPlayerIndex}
            onSelect={(direction) => this.changeSelectedPlayer(direction)}
            stage={stage}
          />
          <div className="player-select__wrapper">
            <PlayerSelection
              selectedPlayer={visiblePlayer}
              playerOne={this.state.playerOne}
              playerTwo={this.state.playerTwo}
              setFighter={() => this.setFighter()}
              weapons={this.props.weapons}
              shields={this.props.shields}
              onWeaponSelected={this.handleWeaponSelection.bind(this)}
              onShieldSelected={this.handleShieldSelection.bind(this)}
              selectedWeapon={this.state.selectedWeapon}
              selectedShield={this.state.selectedShield}
              stage={stage}
            />
          </div>
          { (stage === 'fighters_selected') &&
              <button
                className="start-battle-button"
                onClick={(e) => this.createBattle()}
              >Commencer la bataille !</button>
          }
        </div>
      </div>
    )
  }
}
