class ItemChooser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: this.props.selectedItem
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selectedItem: nextProps.selectedItem
    });
  }

  render() {
    return (
      <div className="item-chooser">
        <h3>{this.props.title}</h3>
        {this.props.items.map((item) => {
          return (
            <button
              key={item.id}
              onClick={() => {
                this.props.onSelected(item);
                this.setState({selectedItem: item})
              }}
              className={classNames(
                'item',
                {'selected': item === this.state.selectedItem}
              )}
              title={this.props.getTooltip(item)}
            >
              {item.name}
            </button>
          )
        })}
      </div>
    )
  }
}
