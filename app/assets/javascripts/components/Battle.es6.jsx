class Battle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fighterOnePoints: this.props.fighters[0]['life_points'],
      fighterTwoPoints: this.props.fighters[1]['life_points']
    }
  }

  componentDidMount() {
    this.initiateBattle();
  }

  initiateBattle() {
    const timer = setInterval(() => {
      const fighterOnePoints = this.state.fighterOnePoints;
      const fighterTwoPoints = this.state.fighterTwoPoints;
      const battleCompleted = (fighterOnePoints <= 0) || (fighterTwoPoints <= 0);
      if (battleCompleted) {
        clearInterval(timer);
        const winnerIndex = (fighterOnePoints <= 0) ? 1 : 0;
        const loserIndex = (fighterOnePoints <= 0) ? 0 : 1;
        this.updateWinnerView(winnerIndex, loserIndex);
        this.saveBattle();
      } else {
        this.executeBattle();
      }
    }, 1000);
  }

  saveBattle() {
    ({battle} = this.props);
    ({fighterOnePoints, fighterTwoPoints} = this.state);

    $.post({
      url: `/battles/${battle.id}/set_result`,
      data: {
        fighter_one_points: fighterOnePoints,
        fighter_two_points: fighterTwoPoints
      }
    });
  }

  updateWinnerView(winnerIndex, loserIndex) {
    const winner = this.props.fighters[winnerIndex];
    const loser = this.props.fighters[loserIndex];
    $(`[data-fighter_id="${winner.id}"] img`).addClass('winner-image');
    $(`[data-fighter_id="${loser.id}"] img`).css('opacity', '0.5');
    this.$newBattleButtonEl.css('visibility', 'visible');
  }

  executeBattle() {
    let fighterOnePoints = this.state.fighterOnePoints;
    let fighterTwoPoints = this.state.fighterTwoPoints;

    const fighterOne = this.props.fighters[0];
    const fighterTwo = this.props.fighters[1];
    const f1Damage = Math.max((fighterOne['attack_points'] - fighterTwo['defense_points']), 1);
    const f2Damage = Math.max((fighterTwo['attack_points'] - fighterOne['defense_points']), 1);

    const attacker = (Math.random() > 0.5) ? 'playerTwo' : 'playerOne';
    if (attacker === 'playerOne') {
      this.movePlayer('one');
      fighterTwoPoints = fighterTwoPoints - f1Damage;
      this.setState({fighterTwoPoints: fighterTwoPoints});
      this.calculateLifePercent('two', fighterTwoPoints);
    } else {
      this.movePlayer('two');
      fighterOnePoints = fighterOnePoints - f2Damage;
      this.setState({fighterOnePoints: fighterOnePoints});
      this.calculateLifePercent('one', fighterOnePoints);
    }
  }

  calculateLifePercent(playerNum, currentPoints) {
    const player = (playerNum === 'one') ? this.props.fighters[0] : this.props.fighters[1];
    const playerMaxPoints = player['life_points'];
    const percentage = Math.floor((currentPoints / playerMaxPoints) * 100);
    $(`.player-${playerNum}__container .life-measure__bar`)
      .css('height', `${percentage}%`);
  }

  movePlayer(num) {
    const attackerClass = `.player-${num}__container`;
    $(attackerClass).addClass('animate');
    setTimeout(function() {
      $(attackerClass).removeClass('animate');
    }, 300)
  }

  getHeader() {
    if (this.state.fighterOnePoints <= 0) {
      return `CHAMPION: ${this.props.fighters[1]['name']} !`
    } else if (this.state.fighterTwoPoints <= 0) {
      return `CHAMPION : ${this.props.fighters[0]['name']} !`
    } else {
      return `${this.props.fighters[0]['name']} vs ${this.props.fighters[1]['name']}`
    }
  }

  render() {
    const fighterOne = this.props.fighters[0]
    const fighterTwo = this.props.fighters[1]
    const header = this.getHeader();
    return (
      <div className="battle__container">
        <h1 className="battle__header default-text-shadow">{header}</h1>
        <div
          className="battle__launch-new"
          ref={(el) => this.$newBattleButtonEl = $(el)}
        >
          <div>
            <a href={this.props.newBattleLink}>
              <button>Lancer une nouvelle bataille !</button>
            </a>
          </div>
          <div>
            <a href={this.props.battleStatsPath}>
              <button>Stats des batailles</button>
            </a>
          </div>
        </div>
        <div className="battle__ring">
          <div
            className="player__container player-one__container"
            data-fighter_id={fighterOne.id}
          >
            <div className="life-measure__container player-1">
              <div className="life-measure__bar"></div>
            </div>
            <img
              ref={(el) => this.$playerOneEl = $(el)}
              src={fighterOne['image_url']}
            />
          </div>
          <div
            className="player__container player-two__container"
            data-fighter_id={fighterTwo.id}
          >
            <img
              ref={(el) => this.$playerTwoEl = $(el)}
              src={fighterTwo['image_url']}
            />
            <div className="life-measure__container player-2">
              <div className="life-measure__bar"></div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
