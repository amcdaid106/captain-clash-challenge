# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Player.delete_all

def save_image(player, avatar_file_name)
  player.avatar = Rails.root.join("db/images/#{avatar_file_name}").open
  player.save!
end

p1 = Player.create(
  name: "Captain Contrat",
  life_points: 12,
  attack_points: 3
)
save_image(p1, "captain_contrat.png")

p2 = Player.create(
  name: "Aoife",
  life_points: 15,
  attack_points: 2
)
save_image(p2, "aoife.jpg")

p3 = Player.create(
  name: "Céline",
  life_points: 18,
  attack_points: 2
)
save_image(p3, "celine.jpg")

p4 = Player.create(
  name: "DHH",
  life_points: 19,
  attack_points: 3
)
save_image(p4, "dhh.png")

p5 = Player.create(
  name: "Matz",
  life_points: 16,
  attack_points: 2
)
save_image(p5, "matz.jpg")

p6 = Player.create(
  name: "Maxime",
  life_points: 15,
  attack_points: 3
)
save_image(p6, "maxime.png")

p7 = Player.create(
  name: "Philippe",
  life_points: 14,
  attack_points: 4
)
save_image(p7, "philippe.png")

p8 = Player.create(
  name: "Flame the Fox",
  life_points: 17,
  attack_points: 2
)
save_image(p8, "firefox.png")

p9 = Player.create(
  name: "Tweeter",
  life_points: 16,
  attack_points: 3
)
save_image(p9, "twitter.png")


Weapon.delete_all

Weapon.create(
  name: "Massue",
  hit_points: 3,
  image: "club.png"
)

Weapon.create(
  name: "Hallebarde",
  hit_points: 8,
  image: "axe.png"
)

Weapon.create(
  name: "Epée",
  hit_points: 5,
  image: "sword.png"
)


Shield.delete_all

Shield.create(
  name: "Celtique",
  defense_points: 4,
  image: "celtic_shield.png"
)

Shield.create(
  name: "Aigle",
  defense_points: 10,
  image: "eagle_shield.jpg"
)

Shield.create(
  name: "Viking",
  defense_points: 5,
  image: "viking_shield.png"
)
