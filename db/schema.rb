# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170822201122) do

  create_table "battles", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fighters", force: :cascade do |t|
    t.integer  "battle_id"
    t.integer  "player_id"
    t.integer  "weapon_id"
    t.integer  "shield_id"
    t.integer  "remaining_life_points"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.boolean  "winner"
    t.index ["battle_id"], name: "index_fighters_on_battle_id"
    t.index ["player_id"], name: "index_fighters_on_player_id"
    t.index ["shield_id"], name: "index_fighters_on_shield_id"
    t.index ["weapon_id"], name: "index_fighters_on_weapon_id"
  end

  create_table "players", force: :cascade do |t|
    t.string   "name"
    t.string   "avatar"
    t.integer  "life_points"
    t.integer  "attack_points"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "shields", force: :cascade do |t|
    t.string   "name"
    t.integer  "defense_points"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "image"
  end

  create_table "weapons", force: :cascade do |t|
    t.string   "name"
    t.integer  "hit_points"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "image"
  end

end
