class AddColumnToFighter < ActiveRecord::Migration[5.0]
  def change
    add_column :fighters, :winner, :boolean
  end
end
