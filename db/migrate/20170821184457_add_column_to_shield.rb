class AddColumnToShield < ActiveRecord::Migration[5.0]
  def change
    add_column :shields, :image, :string
  end
end
