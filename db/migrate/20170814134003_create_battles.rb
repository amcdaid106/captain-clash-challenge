class CreateBattles < ActiveRecord::Migration[5.0]
  def change
    create_table :battles do |t|
      t.integer :player_one_id
      t.integer :player_two_id
      t.integer :player_one_points
      t.integer :player_two_points

      t.timestamps
    end
  end
end
