class RemoveColumnsFromBattle < ActiveRecord::Migration[5.0]
  def change
    remove_column :battles, :player_one_id
    remove_column :battles, :player_two_id
    remove_column :battles, :player_one_points
    remove_column :battles, :player_two_points
    remove_column :battles, :winner
  end
end
