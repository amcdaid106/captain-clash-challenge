class CreateFighters < ActiveRecord::Migration[5.0]
  def change
    create_table :fighters do |t|
      t.references :battle, foreign_key: true
      t.references :player, foreign_key: true
      t.references :weapon, foreign_key: true
      t.references :shield, foreign_key: true
      t.integer :remaining_life_points

      t.timestamps
    end
  end
end
