class RenameColumnInPlayers < ActiveRecord::Migration[5.0]
  def change
    rename_column :players, :avatar_image, :avatar
  end
end
