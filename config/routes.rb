Rails.application.routes.draw do
  root 'pages#home'

  resources :players, only: [:new, :create]

  resources :fighters, only: [:update]

  resources :battles, only: [:index, :new, :show, :create] do
    post :set_result
  end

end

